package u02lab.code;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by elisa.casadio7 on 07/03/2017.
 */
public class RandomGeneratorTest {

    private static final int NUMBER_GENERATED_ELEMENTS = 6;

    private SequenceGenerator generator;

    @Before
    public void init() {
        this.generator = new RandomGenerator(NUMBER_GENERATED_ELEMENTS);
    }

    @Test
    public void isNotOver() {
        this.generator.next();
        assertFalse(this.generator.isOver());
    }

    @Test
    public void isOver() {
        for (int i = 0; i < NUMBER_GENERATED_ELEMENTS; i++) {
            this.generator.next();
        }
        assertTrue(this.generator.isOver());
    }

    @Test
    public void checkProducedElements() {
        List<Integer> listProducedElements = new ArrayList<>();
        for (int i = 0; i < NUMBER_GENERATED_ELEMENTS; i++) {
            listProducedElements.add(this.generator.next().get());
        }
        assertEquals(NUMBER_GENERATED_ELEMENTS, listProducedElements.size());
    }

    @Test
    public void checkNextAfterN() {
        for (int i = 0; i < NUMBER_GENERATED_ELEMENTS; i++) {
            this.generator.next();
        }
        Optional<Integer> nextElement = this.generator.next();
        assertEquals(Optional.empty(), nextElement);
    }

    @Test
    public void resetNextElement() {
        List<Integer> listElement = new ArrayList<>();
        listElement.add(this.generator.next().get());
        this.generator.reset();
        for (int i = 0; i < NUMBER_GENERATED_ELEMENTS; i++) {
            listElement.add(this.generator.next().get());
        }
        assertEquals(NUMBER_GENERATED_ELEMENTS + 1, listElement.size());
    }

    @Test
    public void allRemainingElements() {
        this.generator.next();
        List<Integer> listElement = this.generator.allRemaining();
        assertEquals(NUMBER_GENERATED_ELEMENTS - 1, listElement.size());
    }
}
