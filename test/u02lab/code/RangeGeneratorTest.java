package u02lab.code;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by elisa.casadio7 on 07/03/2017.
 */
public class RangeGeneratorTest {

    private static final int START_ELEMENT = 2;
    private static final int STOP_ELEMENT = 6;
    private static final int EXPECTED_ELEMENT_AFTER_RESET = 2;

    private SequenceGenerator generator;

    @Before
    public void init() {
        this.generator = new RangeGenerator(START_ELEMENT, STOP_ELEMENT);
    }

    @Test
    public void checkProducedElements() {
        List<Integer> elements = new ArrayList<>();
        List<Integer> ranges = new ArrayList<>();
        for (int i = START_ELEMENT; i <= STOP_ELEMENT; i++) {
            elements.add(i);
            ranges.add(this.generator.next().get());
        }
        assertEquals(elements, ranges);
    }

    @Test
    public void checkNextAfterStop() {
        Optional<Integer> nextElement = Optional.empty();
        for (int i = START_ELEMENT; i <= STOP_ELEMENT + 1; i++) {
            nextElement = this.generator.next();
        }
        assertEquals(Optional.empty(), nextElement);
    }

    @Test
    public void resetNextElement() {
        this.generator.next();
        this.generator.reset();
        Optional<Integer> nextElement = this.generator.next();
        assertEquals(Optional.of(EXPECTED_ELEMENT_AFTER_RESET), nextElement);
    }

    @Test
    public void isNotOver() {
        this.generator.next();
        assertFalse(this.generator.isOver());
    }

    @Test
    public void isOver() {
        for (int i = START_ELEMENT; i < STOP_ELEMENT; i++) {
            this.generator.next();
        }
        assertTrue(this.generator.isOver());
    }

    @Test
    public void allRemainingElements() {
        List<Integer> listElements = new ArrayList<>();
        for (int i = START_ELEMENT + 1; i <= STOP_ELEMENT; i++) {
            listElements.add(i);
        }
        this.generator.next();
        List<Integer> allRemainingElements = this.generator.allRemaining();
        assertEquals(listElements, allRemainingElements);
    }
}
